<?php
include_once ('../../../vendor/autoload.php');

use App\Bitm\SEIP125668\Book\Book;

if((isset($_POST['title']))&&(!empty($_POST['title']))) {

    $book = new Book();
    $book->prepare($_POST)->store();
}
else {
    echo "Please Input Data";
}

?>